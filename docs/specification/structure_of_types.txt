                    ��������� �� ��������.


  ���� �������� ������ ����������� �� ��������. ��� � ���������
� ������������� ������������. ����� �� ��������� �� �����
�������, �������� �� ����������� ����� � ������� � ������������
���������� �� �������� ������.


  ������������ �� ����� ��� ��������� � ������� �� ���� �
������� ����. ��������� �������� � �������� "bg". ������
���������� ����� ������������� �� ����. ����������� �� ��������
���� �������� ������� �� �������� � ����:
001, 002, ..., 010, 011, ..., 100, 101 ...
������� �� ����� ������� �� ��� ���������� � ����� ������ ����
���������. �� ���������� �� ������ �� ��������� ������� ��������
�����:
001, 001a, 001b, 002, 003, 003�.
������������ � ".dat". �������� �� �������� ��������:
bg001.dat, bg001a.dat, bg002.dat, bg003.dat, bg003a.dat, ...
�������� �� ������������ � �� ����� �� �� ���������.
������������ ������ �� ��� ��������� ���� bg015.dat ���� � ��
� ������. ����������� �� �� ������������. �� ��� ����� ��� ���
������(���) �� ������� ������ �� ���������� �� ������� ��������
�����.


  ������ � �� �� ���������� ��������� ��������������.
����������� ������� �� 001, ���� ���� �� ��������� �� 001a � ���
�� ��� �� ��������� �� 001b, ��� 001a �� ���������� �� ���������
��� 002 � ��. ����������� �������� ������ ���������� ��� (����
���� � �����, ��� �����) �� ����������.


  �� ������������ � ���������� �� ������� �� ��������
������������ ��������� �� ��������� ������� (���������� �
�������������).


  ������ �������� �� ����������� �� ������������ � ��������,
����� ������� ������ �������:

noun - ������������� (1 - 75)
  male - ����� ��� (1 - 40)
  female - ������ ��� (41 - 53)
  neutral - ������ ��� (54 - 75)
adjective - ������������ (76-89)
pronominal - ����������� (90 - 130)
  personal - ����� (90 - 97)
  demonstrative - ����������� (98 - 105)
  possessive - ������������ (106 - 113)
  interrogative - ������������ (114 - 117)
  relative - ����������� (118 - 120)
  indefinite - ��������������� (121 - 123)
  negative - ����������� (124 - 126)
  general - ����������� (127 - 130)
numeral - ���������� (131 - 141)
  cardinal - ������ (131 - 139)
  ordinal - ����� (140 - 141)
verb - ������� (142 - 187)
various - (188 - 198) �����, ����� �� ���� � �������
  adverb - ������� (188)
  conjunction - ����� (189)
  interjection - ���������� (190)
  particle - ������� (191)
  preposition - �������� (192)
  months - ������ (193)
  names - ����� � ���������� ����� (194 +)
    bg_towns - ��������� �������� ����� (195)
    bg_various - ��������� ���������� ������� (194)
    capitals - ������ �� ��������� (197)
    cities - ������ �� ������������������ ������� (198)
    countries - ������ �� �������� (196)
    various - ����� ���������� ������� (199)
    names_and_terms - ����� � �������� ����� (200)
    names_and_families - ����� � ������� (201 - 207)
      families - ������� ����� (201 - 204)
      names - ����� ����� (205 - 207)


  ������ ��������� � ��������� �� �������� � ����� �������.

1. ������ ������ �� ������� �� ������������� �������� ���� ��
���� ���� (�����������). ����� ����������� ������� �� ���
������� �����. ������� �� ��������� � ���� ���� ������� ��
�������� � ���� ������.

2. �������� �� ������� �� ������������ ���� �� ��������� � ����
���� ����� �� �� �������.

3. �������� ���� 187 �� ��� ��������� � �� ���� ������� �
�������. ������ �� ���� ������ ����� �����. �� ���� ���� ����
������� �����. ����� "��" �� � "��" �� ������ ���� � �����.
������ ���� ����� �� ����� ������� � ���� ����. �� ������ � ��
�� ������� ���������� � ����� ���� ����� �� �����.

4. ������������ ������� � ������� � ��������� �� �������������
� ������� ����� � ��� ��-����� ������� � �������� �� ���� ����.

5. ������ ���� �� ������ �� 193 ����������� �������� � �����
�����, � ���� 193 � ������ �����.


  ��� ����� ���� ���������� ��� ���� description.dat, �����
������ ���� �� ������ � ���������� �����. ���� ������� �� ��
�������� ��� ������������ �� ��������� �� �������� �� ���������.
������������ �� ���������� � ��� ����������� �� ������� ��
�����.


  ���������� �� ����������� �� ��������� � ����� � ��������� �
�������� � �������� � ���� ��������.
